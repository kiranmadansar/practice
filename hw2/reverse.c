/*  File Name:   reverse.c
    Description: Given an array of characters, reverse it using only pointers.
    Author Name: Kiran Hegde
    Date: 09/21/2017
    Tools Used:  VIM editor, GNU GCC compiler
*/



#include <stdio.h>

// -----------------function to reverse an array of characters -----------------
char reverse(char *str, int length)
{
    char temp[length];
    int j, k;
    for(j=length; j>0; j--)
    {
        temp[j]=*str;
        str++;
    }
    for(k=length+1; k>0; k--)
    {
        *str = temp[k];
        str--;
    }

    return 0;
}
//------------------ reverse function ends here ----------------------------------

/*
//-------------function to find the length ------------------------------------------
int lengthfind(char *array1)
{   int len=0;
    while(*array1!= '\0' )                // increment the count until pointer goes to null
    {                                     // i.e end of array
        ++len;
        array1++;
    }
    return len;                           // return length
}
//------------- length find function ends here --------------------------------------
*/


int main()
{
    int i=0, j=0, result=2, result1=2, result2=2, x=0, y=0;  // declare and initialize variables
    char array1[] = "This is a string.";                //input array that has to be reversed
    int arr1len = 17;
    char array2[] = "some NUMmbers12345";
    int arr2len = 18;
    char array3[] = "Does it reverse \n\0\t correctly?";
    int arr3len = 30;

    //i = lengthfind(&array1);                        // length calculated
    printf("String Before reversing \n\n");
	for(y=0; y<arr1len; y++)                      // print array1 before reversing
    	{
        	printf("%c", array1[y]);
    	}
    	printf("\n");

        for(y=0; y<arr2len; y++)                      // print array2 before reversing
        {
                printf("%c", array2[y]);
        }
        printf("\n");

        for(y=0; y<arr3len; y++)                      // print array3 before reversing
        {
                printf("%c", array3[y]);
        }
        printf("\n\n\n");


    result = reverse(array1, arr1len);              // call reverse function
    result1 = reverse(array2, arr2len); 
    result2 = reverse(array3, arr3len); 


	printf("String After reversing\n\n");
	for(x=0; x<arr1len; x++)                      // print array1 after reversing
    	{
        	printf("%c", array1[x]);
    	}
        printf("\n");

        for(x=0; x<arr2len; x++)                      // print array2 after reversing
        {
                printf("%c", array2[x]);
        }
        printf("\n");

        for(x=0; x<arr3len; x++)                      // print array3 after reversing
        {
                printf("%c", array3[x]);
        }
        printf("\n\n\n");


    printf("Error code for array1= %d \n", result);    //print error code
    printf("Error code for array2= %d \n", result1);
    printf("Error code for array3= %d \n", result2);
    return 0;
}

