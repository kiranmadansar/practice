/*  File Name:   memory.c
    Date :       09/21/2017
    Description: Changing the data at different memory location to understand pointers 
    Author Name: Kiran Hegde
    Tools Used:  VIM editor, GNU GCC compiler
*/


#include <stdio.h>

int main()
{
	int i;	unsigned int arr[8];
	int *ptr = arr;
	// printf("Addr: %d\n \n", ptr);
	unsigned int x[8];
	
	arr[0]=0x0EE;
        arr[1]=0x012;
        arr[2]=0x77;
        arr[3]=0x0BE;
        arr[4]=0x066;
        arr[5]=0x054;
        arr[6]=0x033;
        arr[7]=0x0f0;

	for(i=0; i<8; i++)
	{
		x[i]=arr[i];	//Since pointers alters the original data,
	}			//here I am making a copy 
	
	i=0;

	*ptr = arr[0];		// given procedure
	*ptr = 0xF1 & 127;
	ptr++;
	*ptr += 17;
	ptr += 2;
	*ptr = 15%4;
	ptr--;
	*ptr >>=4;
	ptr = &arr[5];
	*ptr = (1<<5) | (4<<2);
	*((char *)&arr[7]) = 22;
	
	ptr = &arr[0];
	
//-------------------- printing table here with address, data and data after ------------------------
								
	printf("____________________________________\n");	
	printf("  Address   |   Data  | Data after | \n");
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	for (i = 0; i<8; i++)
	{
		printf("%10d  |     %2x  |      %2x    |\n", (ptr+i),x[i], *(ptr+i));
	}
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
//------------------ finished printing --------------------------------------------------------------
	return 0;
}
