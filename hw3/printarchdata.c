/*  File Name:   printarchdata.c
    Date :       09/21/2017
    Description: Displaying size of different datatypes
    Author Name: Kiran Hegde
    Tools Used:  VIM editor, GNU GCC compiler
*/


#include <stdio.h>
#include <stdint.h>

//-------------------- Function to print size of datatypes starts ---------------------------
void print_arch_data()
{
		
	printf("Size of char		: %lo bytes \n", sizeof(char));
	printf("Size of int		: %lo bytes \n", sizeof(int));
	printf("Size of float		: %lo bytes \n", sizeof(float));
	printf("Size of double		: %lo bytes \n", sizeof(double));
	printf("Size of short		: %lo bytes \n", sizeof(short));
	printf("Size of long		: %lo bytes \n", sizeof(long));
	printf("Size of long int	: %lo bytes \n", sizeof(long int));
	printf("Size of long long	: %lo bytes \n", sizeof(long long));
	printf("Size of int8_t 		: %lo bytes \n", sizeof(int8_t));
	printf("Size of uint8_t 	: %lo bytes \n", sizeof(uint8_t));
	printf("Size of uint16_t	: %lo bytes \n", sizeof(uint16_t));
	printf("Size of uint32_t 	: %lo bytes \n", sizeof(uint32_t));
	printf("Size of char * 		: %lo bytes \n", sizeof(char *));	
	printf("Size of int * 		: %lo bytes \n", sizeof(int *));
	printf("Size of float * 	: %lo bytes \n", sizeof(float *));
	printf("Size of void * 		: %lo bytes \n", sizeof(void *));
	printf("Size of void ** 	: %lo bytes \n", sizeof(void **));
	printf("Size of int8_t * 	: %lo bytes \n", sizeof(int8_t *));
	printf("Size of int16_t * 	: %lo bytes \n", sizeof(int16_t *));
	printf("Size of int32_t * 	: %lo bytes \n", sizeof(int32_t *));
	printf("Size of size_t 		: %lo bytes \n", sizeof(size_t));
}
//------------------- function ends here --------------------------------------------------


int main()
{
	print_arch_data(); //call the function
	return 0;
}
